//
/************************************************

John Chimento
jchimen
Lab5
Section 2
Hanjie Liu

*************************************************/

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include "stdlib.h"
#include "string.h"
using namespace std;
enum Suit { SPADES=0, HEARTS=1, DIAMONDS=2, CLUBS=3 };

typedef struct Card {
  Suit suit;
  int value;
} Card;

string get_suit_code(Card& c);
string get_card_name(Card& c);
bool suit_order(const Card& lhs, const Card& rhs);
int myrandom (int i) { return std::rand()%i;}


int main(int argc, char const *argv[]) {
  // IMPLEMENT as instructed below
  /*This is to seed the random generator */
  srand(unsigned (time(0)));

	int s = 0, h = 0, d = 0, c = 0;

  /*Create a deck of cards of size 52 (hint this should be an array) and
   *initialize the deck*/
	Card deck [52];
	for(int i=0; i<52; i++) {
		
		if(i < 14) {
			deck[i].suit = SPADES;
			deck[i].value = s;
			s++;
		}
		
		if(i >=14 && i < 28) {
			deck[i].suit = HEARTS;
			deck[i].value = h;
			h++;
		}
		
		if(i >=28 && i < 40) {
			deck[i].suit = DIAMONDS;
			deck[i].value = d;
			d++;
		}
		
		if(i >=40 && i < 52) {
			deck[i].suit = CLUBS;
			deck[i].value = c;
			c++;
		}

					
}
		



  /*After the deck is created and initialzed we call random_shuffle() see the
   *notes to determine the parameters to pass in.*/

//random_shuffle()
	

   /*Build a hand of 5 cards from the first five cards of the deck created
    *above*/

Card hand[5] = {deck[0], deck[1], deck [2], deck[3], deck[4]};


for(int j=0; j<5; j++) {
  std::cout << hand[j].value << endl;
}


    /*Sort the cards.  Links to how to call this function is in the specs
     *provided*/


    /*Now print the hand below. You will use the functions get_card_name and
     *get_suit_code */




  return 0;
}


/*This function will be passed to the sort funtion. Hints on how to implement
* this is in the specifications document.*/

bool suit_order(const Card& lhs, const Card& rhs) {
  
   if(lhs.suit < rhs.suit) {
	return true; 
}

   else if(lhs.suit > rhs.suit) {
	return false; 
}
   
   else {
	return lhs.value < rhs.value;
	}
}



string get_suit_code(Card& c) {
  switch (c.suit) {
    case SPADES:    return "\u2660";
    case HEARTS:    return "\u2661";
    case DIAMONDS:  return "\u2662";
    case CLUBS:     return "\u2663";
    default:        return "";
  }
}

string get_card_name(Card& c) {
  // IMPLEMENT
}
